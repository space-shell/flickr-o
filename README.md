# flickr-o

A single page application developed in react to retrieve and display images from Flickr! The application randomly selects a set of Tags listed in a configuration file to view images from.

[![pipeline status](https://gitlab.com/spaceshell_j/nasa-library/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/flickr-o/commits/master)

[Demo](http://flickr-o.s3-website-eu-west-1.amazonaws.com/) - :link:

[API Endpoint](https://f4tzo8q7z4.execute-api.eu-west-1.amazonaws.com/prod) - :link:

[Repository](git@gitlab.com:spaceshell_j/flickr-o.git) - :link:

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Libraries](#Libraries)
* [Technical Details](#TechnicalDetails)
* [Authors](#Authors)

## Installation

To install, you can use [npm](https://npmjs.org/)

```bash
$ npm install
```

## Usage

To start the WebPack development server run the following command

```bash
$ npm run start:dev
```

And to Build the distributable version...

Example:

```bash
$ npm run build:webpack
```

To deploy the distributable to an AWS S3 bucket simply adjust the following credentials in the `package.json` file to match your configuration.

```bash
$ aws s3 sync ./dist s3://flickr-o --acl public-read --profile default
```

> The current configuration runs Jest as a CI tool for a GitLab Server

## Libraries

The project comes with a few library items that support the spaceshell

#### flickr-api-key

---

An AWS Lambda function deployed to scrape the Flickr.com main page for their public API key. The Lambda collects the key and caches it against a corresponding expiration date.

Adjust the following in the `package.json` file to allow NPM to deploy the function to AWS account of your choice.

```bash
$ cd src/library/flickr-api-key && 7z a -r index.zip index.js && aws lambda update-function-code --function-name flickr-api-key --zip-file fileb://index.zip
```

#### url-queries

An interface between JavaScript Object and URL query notation. the module manipulates query strings by adding replacing or subtracting keys and values.

**Example**

```JavaScript
import UrlQueries from './url-queries'

const params =  {
  'method': 'flickr.photos.getInfo',
  'api_key': 'b4d835bc68af7801e7ab25c502b85342',
  'photo_id': '35917219690',
  'format': 'json',
  'nojsoncallback': '1'
}


const test = UrlQueries('https://api.flickr.com/services/rest/', params)

test
  .queriesAdd(['Bish', 'Bash', 'Bosh']) // Add and array of strings
  .queriesRemove(['Bish', 'Bosh']) // Remove key names as a String or Array of strings
  .queriesAdd({ ['good morning']: 'morning', fizz: 'bang' }) // Add an Object of Strign KVP's
  .queriesAdd({ ['good morning']: '???', fizz: '!!!' }) // Overwrites existing values
  .getQueryUrl() // Return string value

```

#### flickr-data (Depreciated)

---

An obsolete package for handling seemingly random JSON-P API calls. The function strips the padding from the response and returns a promise object containing any internal data. The module also manages multiple synchronous API calls as well as duplicate filtration.


## Technical Details

The designs wireframe has ben based off of an existing web design but for this project I have played with the style to create a semi flat look and feel. the Images are featured as 'Cards' with only a slight thickness to them.

The project has been build in such a was that is most comfortable for me and I have decided to use ReactJS as a framework partly since I have the most experience with ReactJS and partly since it's modular nature makes it a good candidate for the task.

Compilation and transpillation is handled by WebPack primarily which utilises babel to transform the Es6 / 7 syntax to ES5 and to also process the React's JSX code.

The main challenge for this project was to plan a system that could switch between various sets of images and maintain, in memory, a list of images to be favorited. Using Flickr's API end points I could extract all the information I needed and more, such as response limits, id tags and scalable images.

**Future development**

There are a few considerations for future development as follows:

 - 'Request more' - To append more image results to the existing set
 - 'Lazier Loading' - Have each card request the base64 encoded image data for optimization purposes and to help handle more image requests as mentioned above


## Authors

*James Nicholls*
