import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Route, Switch } from 'react-router-dom'

import Home from './pages/home'
import Favourites from './pages/favourites'

import Header from './modules/header'
import Footer from './modules/footer'

import Nav from './components/nav'

import UrlQueries from './library/url-queries/'

import flickr_config, { Tags } from './flickr.config.js'

import './app.less'


export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      apiKey: (sessionStorage.getItem('flickr_api')||''),
      flickrUrl:  UrlQueries('https://api.flickr.com/services/rest', flickr_config), // UrlQueries to handle query updates
      title: Object.keys(Tags)[~~(Math.random()*Object.keys(Tags).length)],
      active: {},
      loading: true
    }
  }

  // API endpoint decleration
  componentDidMount = () => {
    fetch('https://f4tzo8q7z4.execute-api.eu-west-1.amazonaws.com/prod') // Retreive latest API key
      .then(resp => resp.text())
      .then(key => {
        key = key.replace(/[""]/g, '') // TODO Revise response body mapping
        sessionStorage.setItem('flickr_api', key) // Set Flickr Session key to storage
        this.setState({
          active: this.tagsActivate(),
          flickrUrl: this.state.flickrUrl.queriesAdd({
            api_key: key,
            tags: Tags[this.state.title].join(',') // Join all tags to string
          })
        }, () => {
          this.getData()
        })
      })
  }

  tagsActivate = () => {
    return Tags[this.state.title].reduce((obj, tag) => {
      obj[tag] = true
      return obj
    }, {})
  }

  // On tag change, update
  tagsUpdate = () => {
    const searchTags = Object.keys(this.state.active) // Builds the tags into a string
      .reduce((str, tag, idx) => {
        if(idx) str += ','

        return str += this.state.active[tag] ? `${tag}` : ''
      }, '')

    const url = this.state.flickrUrl
      .queriesAdd({ tags: searchTags }) // Updates the query string with the appropriote tags

    this.setState({
      flickrUrl: url
    }, () => this.getData())
  }

  // cacheData = (data) => { // Used for Caching all objects into the state
  //   const ids = this.state.dataCache.map(k => k.id)
  //   const filtered = data.filter(d => {
  //
  //     return !ids.includes(d.id)
  //   })
  //   this.setState({
  //     dataCache: [
  //       ...this.state.dataCache,
  //       ...filtered
  //     ]
  //   })
  // }

  getData = () => {
    fetch(this.state.flickrUrl.getQueryUrl()) // Dynamically build query for fetch
      .then(resp => resp.json())
      .then(flickr => {
        this.setState({ // NOTE Hard coded photos key
          imageData: flickr.photos.photo,
          loading: false
        })
      }).catch(err => {
        console.log(err)
      })
  }

  navigtionItems = () => {
    return Tags[this.state.title].map(child => (
      <div
        key={ `tag-${child}` }
        className={ this.state.active[child]
          ? 'nav-item active'
          : 'nav-item'
        }
        onClick={ () => this.setState({ // Toggle tag active
          active: {
            ...this.state.active,
            [child]: !this.state.active[child]
          }
        }, this.tagsUpdate(child) )}
      >
        { child }
      </div>
    ))
  }

  render = () => {
    return (
      <main>
        <Header title={ this.state.title } />
        <Nav>
          { this.navigtionItems() }
        </Nav>
        <Switch>
          <Route exact path="/" render={() => (
            <Home imageData={ this.state.imageData }/> // Propagate image data down
          )} />
          <Route path="/favourites" render={() => (
            <Favourites ></Favourites >
          )} />
          <Route component={Home} />
        </Switch>
        <Footer />
      </main>
    )
  }
}
