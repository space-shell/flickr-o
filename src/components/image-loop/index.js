import React, { Component } from 'react'

import './main.less'

export const ImageLoop = ({image, title}) => {

  return (
    <div
      id='ImageLoop'
      style={{ backgroundImage: `${image}` }}
    >
      <div className='title'>
        { title }
      </div>
    </div>
  )
}

export default ImageLoop
