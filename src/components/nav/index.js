import React, { Component } from 'react'

import './main.less'

// Creates a stick nav bar
export default class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sticky: false
    }
  }

  // Component handles the sticking unsticking action
  componentDidMount = () => {
    this.setState({ top: this.navBar.offsetTop })
    window.addEventListener('scroll', this.navSticky);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.navSticky);
  }

  // Sets state to weather nav is sticky or not
  navSticky = e => {
    const stuck = e.target.scrollingElement.scrollTop > this.state.top

    if(this.state.sticky !== stuck) this.setState({ sticky: stuck }) // Toggle sticky
  }

  render = () => (
    <nav
      ref={(navBar) => { this.navBar = navBar; }}
      className={ this.state.sticky ? 'sticky' : ''}
    >
      { this.props.children }
    </nav>
  )
}
