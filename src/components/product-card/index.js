import React, { Component } from 'react'

import UrlQueries from '../../library/url-queries'

import './main.less'

export default class ProductCard extends Component {
  constructor(props){
    super(props)
    this.state = {
      ...this.props,
      loading: true,
      favourite: false
    }
  }

  // Checks for favourited state in local storage
  componentDidMount = () => {
    const fav = this.getLocalStorage('favourites')
    if(this.state.id in (fav||{})) this.setState({
      favourite: fav[this.state.id]
    })
  }

  // Update local storage function
  updateLocalStorage = (key, val) => {
    let localOld = sessionStorage.getItem(key)

    localOld = JSON.parse(localOld)

    try {
      const localNew = JSON.stringify({...localOld, ...val})

      sessionStorage.setItem(key, localNew)
      console.log(`Session item: ${key}, has been set`)
    } catch(e) {
      console.error(e)
    }
  }

  getLocalStorage = (key) => {
    return JSON.parse(sessionStorage.getItem(key))
  }

  favouritesToggle = () => {
    this.setState({
      favourite: !this.state.favourite
    }, () => {
      this.updateLocalStorage('favourites', { [this.state.id]: this.state.favourite })
    })
  }

  render = () => {
    return (
      <div id='ProductCard'>
          <div
            className='image'
            style={
              this.state.image && { // Set the 'Loading' Background
                backgroundImage: `url(${this.state.image})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center'
              }
            }
          />
        <div className='bar'>
          <div
            onClick={ this.favouritesToggle }
            style={{ cursor: 'pointer' }}
            className={
              this.state.favourite // Toggle favourite styling
                ? 'fav true'
                : 'fav'
            } />
          <div className='title'>
              { this.state.title }
          </div>
        </div>
        <div className='desc'>
          { this.state.desc }
          <span>
            { this.state.auth }
          </span>
        </div>
      </div>
    )
  }
}
