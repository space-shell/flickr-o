export default {
  'sort': 'relevance',
  'parse_tags': 1,
  'extras': 'url_c,url_m,owner_name,description',
  'tags': 'Patterns',
  'per_page': 25,
  'page': 1,
  'method': 'flickr.photos.search',
  'api_key': '64fa579bbc42f5a3336329df766f213d',
  'format': 'json',
  'nojsoncallback': 1
}

// Sets the random / Dynamic navigation bar items and app theme
export const Tags = {
  Nature: ['sunset', 'weather', 'beach', 'water'],
  Colours: ['red', 'blue', 'green', 'yellow'],
  Patterns: ['tessellate', 'geometric', 'symmetry', 'fractal'],
  Photography: ['portrait', 'landscape', 'people', 'place'],
  Pets: ['dog', 'cat', 'bird', 'fish']
}
