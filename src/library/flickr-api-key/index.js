const https = require('https')
const AWS = require('aws-sdk')

const lambda = new AWS.Lambda();

exports.handler = (evt, ctx, cb) => {

  // Flickr Main page for API key extraction
  https.get('https://www.flickr.com/search/?tags=Hello%20World', (resp) => {
    let data = ''

    resp.on('data', (chunk) => data += chunk)

    resp.on('end', () => {
      const now = ~~(new Date().getTime()/1000)

      console.log(process.env.EXP_DATE > now) // Check time now agains Key Expiry
      if(process.env.EXP_DATE > now) return cb(null, process.env.API_KEY) // Return Cached Key

      const response = extractKvp(data)
      console.log(response)

      lambda.updateFunctionConfiguration({
        FunctionName: 'flickr-api-key',
        Environment: {
          Variables: {
            'API_KEY': String(response.site_key),
            'EXP_DATE': String(response.site_key_expiresAt) // Set latest key as an Env Var
          }
        }
      }, function(err, data) {
        if(err) ctx.fail("Error: " + err)
        else cb(null, process.env.API_KEY)
      })
    })

  }).on("error", (err) => {
    ctx.fail(("Error: " + err.message))
  })

  // Finds the specific API key string and returns as an array
  function extractKvp(text) {
    return text.match(/(root.YUI_config.flickr.api.site_key)\.*[^;]*/g).reduce(function(obj, match){
      match = match.replace('root.YUI_config.flickr.api.', '')
      match = match.replace(/["]/g, '')
      match = match.split(' = ')

      let kvp = {}

      try {
        kvp = JSON.parse('{"' + match[0] + '":"' + match[1] + '"}')
      } catch(e) {
        console.log(e)
      }

      return Object.assign(obj, kvp)
    }, {})
  }
}
