import fetch from 'node-fetch'

import UrlQueries from '../url-queries'

export default function FlickrData(url, count = 1) {
  if(!url) throw new Error('Url not defined')

  // Populates an array with fetch objects
  function dataFill(length) {

    return [...Array(length)].map(arr => dataFetch(url))
  }

  // Validate responses
  function status(response){
    if(response.status >= 200 && response.status < 300) return response

    throw new Error(response.statusText)
  }

  function dataFetch(uri, inner = 'items') {

    return fetch(uri)
      .then(resp => status(resp)) // Response Status check
      .then(data => data.json())
      .then(json => (json[inner]||json)) // Return inner Object 'Optional'
      .catch(err => { throw new Error(err) })
  }

  // Flattens promise data into an Array
  function dataCollect(promises) {

    return Promise.all(promises).then(dat => {

      return dat.reduce((arr, dat) => arr.concat(dat), [])
    })
  }

  return {
    data: [...dataFill(count)], // Initialise factory return
    dataAdd(count = 1) { // Adds more image data
      this.data.push(...dataFill(count))

      return this
    },
    dataFilter(filter, values){ // Filters data for duplicates and custom queries
      const doubles = [].concat(values)
      this.data = this.data.map(dat => { // Map over promise objects

        return dat.then(d => d.filter(filt => { // Map over promise data
          if(doubles.includes(filt[filter])) return false
          doubles.push(filt[filter])

          return true
        }))
      })

      return this
    },
    getData() { return dataCollect(this.data) } // Returne Flattened data
  }
}
