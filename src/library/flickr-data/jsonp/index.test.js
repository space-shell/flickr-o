import FlickrData from './'

// const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags=dogs%20in%20hats'

// const url = 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=10'

// const url = 'https://api.flickr.com/services/rest?sort=relevance&parse_tags=1&extras=url_c%2Curl_m%2Cowner_name%2Cdescription&text=red%20cars&per_page=25&page=1&method=flickr.photos.search&api_key=d0cceeae8549fbfc970cf02b687d7b20&format=json&nojsoncallback=1'

const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=flickrcb&tags=dogs%20in%20hats'

const test = FlickrData(url)

/* QUOKKA */

const Q = FlickrData(url)

Q
  .dataAdd()
  .dataAdd()
  .dataAdd(5)
  //.dataFilter('ID', [...Array(8000)].map(a => ~~(Math.random()*1000)))
  .dataFilter()
  .data

/* QUOKKA */

describe('Testing Flickr Data moule', () => {

  it('Throws no URL Error', () => {
    expect(() => FlickrData()).toThrowError('Url not defined')
  })
  it('Throws Response Error', () => {
    expect(() => FlickrData('https://www.Helloooo.world')).toThrowError(Error)
  })
  it('Returns JSON Object', () => {
    expect(typeof test === 'object')
	})
  it('Gets more objects', () => {
    expect(test)
	})
  it('Removes duplicates', () => {
    expect(test)
	})
  it('Returns a determined number of objects', () => {
    expect(test)
	})
  it('Extracts relevant items', () => {
    expect(test)
	})
  it('Removes specified items', () => {
    expect(test)
	})
})
