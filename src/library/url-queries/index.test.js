import UrlQueries from './'

import MD from './mock.data.js'

/* QUOKKA */

const test = UrlQueries('Hello', ['World', 'wide', 'web'])

// test
//   .queriesAdd(['Bish', 'Bash', 'Bosh']) // Add and array of strings
//   .queriesRemove(['Bish', 'Bosh']) // Remove key names as a String or Array of strings
//   .queriesAdd({ ['good morning']: 'morning', fizz: 'bang' }) // Add an Object of Strign KVP's
//   .queriesAdd({ ['good morning']: '???', fizz: '!!!' })
//   .getQueryUrl() /*?*/

const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=flickrcb&tags=dogs%20in%20hats'
const check = UrlQueries(url).queryParts

check

/* QUOKKA */

describe('Tesing the Query Build function', () => {
  const test = UrlQueries(MD.url, MD.queries)

  it('Constructs factory', () => {
    expect(typeof test === 'object').toBeTruthy()
  })
  it('Checks for base URL', () => {

    expect(() => UrlQueries()).toThrowError('Base URL Required')
  })
  it('Returns string', () => {
    expect(typeof test.getQueryUrl() === 'string').toBeTruthy()
    expect(test.getQueryUrl()).toMatch(MD.url)
    expect(test.getQueryUrl()).toMatch(MD.queries[0])
  })
  it('Accepts query strings / arrays', () => {
    const str = 'Wow!'
    const check = UrlQueries(MD.url, str)
    expect(typeof test.getQueryUrl() === 'string').toBeTruthy()
  })
  it('Replaces spaces with %20', () => {
    const str = 'Hello old friend'
    const check = UrlQueries(MD.url, [str, str])
    expect(check.getQueryUrl()).not.toMatch(/[ ]/)
    expect(check.getQueryUrl()).toMatch(/%20/)
  })
  it('Appends first query with ?', () => {
    expect(test.getQueryUrl()).toMatch(/\?/)
    expect(/\?/.exec(test.getQueryUrl()).length).toBe(1)
  })
  it('Appends subsequent queries with &', () => {
    expect(test.getQueryUrl()).toMatch(/&/g)
    expect(test.getQueryUrl().match(/&/g).length).toBe(MD.queries.length-1)
  })
  it('Adds queries', () => {
    const arr = ['God', 'Save', 'The', 'Queen']
    const check = UrlQueries(MD.url, MD.queries).queriesAdd(arr).getQueryUrl()
    expect(check).toMatch(/&/)
    expect(check.match(/&/g).length).toBe((MD.queries.length-1)+arr.length)
    expect(check.match(/(God|Save|The|Queen)/g).length).toBe(arr.length)
  })
  it('Removes queries', () => {
    const check = UrlQueries(MD.url, MD.queries)
      .queriesRemove(MD.queries[MD.queries.length-1])
      .queryStrings

    const objectArray = check.map(c => Object.keys(c).pop())
    //expect(check).toEqual(MD.queries.slice(0, -1))
    expect(objectArray).toEqual(MD.queries.slice(0, -1))
  })

  describe('Tesing key value pairs', () => {
    it('Converts arrays to arrays of KVP Objects', () => {
      const check = UrlQueries(MD.url, MD.queries)
        .queriesAdd(['Bish', 'Bash', 'Bosh'])
        .queriesRemove(['Bish', 'Bosh'])
        .queryStrings
      expect(check).toEqual(expect.arrayContaining([{ ['bash']: undefined }]))
      //expect(check).not.toEqual(expect.arrayContaining([{ ['Bish']: undefined }]))
    })

    it('Splits KVP Object into an array of KVP Objects', () => {
      const check = UrlQueries(MD.url, MD.queries)
        .queriesAdd(['Bish', 'Bash', 'Bosh'])
        .queriesAdd({ ['good morning']: 'morning', fizz: 'bang' })
        .queriesRemove(['Bish', 'Bosh', 'fizz'])
        .queryStrings
        expect(check).toEqual(expect.arrayContaining([{ ['good morning']: 'morning' }]))
        expect(check).not.toEqual(expect.arrayContaining([{ ['fizz']: 'bang' }]))
    })
  })

  it('Generates string from KVP', () => {
    const check = UrlQueries(MD.url, MD.queries)
      .queriesAdd(['Bish', 'Bash', 'Bosh'])
      .queriesAdd({ ['good morning']: 'morning', fizz: 'bang' })
      .queriesRemove(['Bish', 'Bosh', 'fizz'])
      .getQueryUrl()
    expect(check).toMatch(/(Bash|good morning)/)
  })

  it('Generates object from URL query string', () => {
    const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=flickrcb&tags=dogs%20in%20hats'
    const check = UrlQueries(url).getQueryObjectFromUrl()
    expect(check).toEqual(expect.objectContaining({
      address: expect.any(String),
      queries: expect.any(Object)
    }))
  })
})
