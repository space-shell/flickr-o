import React from 'react'
import { Link } from 'react-router-dom'

import ImageLoop from '../../components/image-loop'

import './main.less'

export const Header = ({ title }) => {
  return (
    <header>
      <div className='title-bar'>
        <Link className='header-test'to='/'>Flickr - O</Link>
        <Link className='favs'to='/favourites'>Favourites</Link>
      </div>
      <div className='link-bar'/>
      <div className='main-image'>
        <div className='title' children={ title }/>
      </div>
    </header>
  )
}

export default Header
