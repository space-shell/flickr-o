export default {
  'method': 'flickr.photos.getInfo',
  'extras': 'url_c,url_m,owner_name,description',
  'api_key': 'b4d835bc68af7801e7ab25c502b85342',
  'photo_id': '35917219690',
  'format': 'json',
  'per_page': '1',
  'nojsoncallback': '1'
}
