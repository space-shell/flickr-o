import React, { Component } from 'react'

import UrlQueries from '../../library/url-queries/'

import ProductCard from '../../components/product-card/'

import './main.less'

import flickr_conf from './flickr-api.conf.js'


export default class Favourites extends Component {
  constructor(props) {
    super(props)
    this.state = {
      apiKey: (sessionStorage.getItem('flickr_api')||''),
      filterCache: [],
      loading: true
    }
  }

  componentDidMount() {
    this.favouritesBuild()
  }


  favouritesBuild() {
    const ids = JSON.parse(sessionStorage.getItem('favourites')) // Collect Favs from session storage
    const idKeys = Object.keys((ids||{})).reduce((arr, item) => { // If keys, retrun true values only
      if(ids[item]) return arr.concat(item)
    }, [])

    const fetchList = idKeys.map((m, idx) => {
      const urls = UrlQueries('https://api.flickr.com/services/rest/', flickr_conf)
        .queriesAdd({ api_key: this.state.apiKey }) // Update with local API Key
        .queriesAdd({ photo_id: idKeys[idx] })
        .getQueryUrl()

      return fetch(urls).then(resp => resp.json())
    })

    Promise.all(fetchList).then(data => { // Async daata calls
      this.setState({
        loading: false,
        filterCache: [
          ...this.state.filterCache,
          ...data
        ]
      })
    })
  }

  generateProductCards() {
    return this.state.filterCache.map(p => {
      console.log(p)
      return (
        <ProductCard
          key={ `fav-${p.photo.id}` }
          id={ p.photo.id }
          title={ p.photo.title['_content'] }
          description=''
          image={ p.photo.url_m }
          auth={ p.photo.ownername }
        />
      )
    })
  }

  render() {
    return (
      <div
        id="Favourites"
        className={ this.state.loading ? 'loading' : '' }
      >
        { !this.state.loading && this.generateProductCards() }
        <div className='more'/>
      </div>
    )
  }
}
