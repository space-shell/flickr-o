import React, { Component } from 'react'

import ProductCard from '../../components/product-card/'

import './main.less'


export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      imageData: this.props.imageData,
      loading: true
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      loading: false,
      imageData: nextProps.imageData
    })
  }

  generateProductCards() {
    return this.state.imageData.map(card => (
      <ProductCard
        key={ `pc-${card.id}` }
        id={ card.id }
        title={ card.title }
        description={ card.description['_content'] }
        image={ card.url_m }
        auth={ card.ownername }
      />
    ))
  }

  render() {
    return (
      <div
        id="Home"
        className={ this.state.loading ? 'loading' : '' }
      >
        { this.state.imageData && this.generateProductCards() }
        <div className='more'/>
      </div>
    )
  }
}
