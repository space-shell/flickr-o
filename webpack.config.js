const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const lessGlobals = require(path.resolve(__dirname, 'less.globals.js'))

const sauce = path.resolve(__dirname, 'src')

const extractLess = new ExtractTextPlugin({
    filename: "[name].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        include: sauce,
        use: extractLess.extract({
          use: [
            { loader: 'css-loader' },
            {
              loader: 'less-loader',
              query: {
                globalVars: lessGlobals
              }
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.js$/,
        include: sauce,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          plugins: ['transform-class-properties'],
          presets: [ 'env', 'react', 'stage-3' ]
        }
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'images/'
        }
      }
    ]
  },
  watchOptions: {
    aggregateTimeout: 1000,
    poll: 1000
  },
  devtool:'source-map',
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    extractLess,
    new HtmlWebpackPlugin({
      hash: true,
      template: 'build/index.ejs',
    })
  ]
}
